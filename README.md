# Saé 1.04 - 2.04 Introduction et exploitation de base de données

### Pour le 1er semestre:

L’objectif de la SAE 1.04 était la modélisation d’une base de données, nous avons du réaliser le MCD d’une base de données pour une banque en ligne, puis le MLD et enfin implémenter cette base sur Access en y ajoutant des données. Nous avons ensuite du réaliser des requêtes sur cette base de données, ainsi que des formulaires et des états.

### Pour le 2eme semestre: 

L’objectif de la SAE 2.04 était cette fois-ci l’exploitation des données et des bases de données. Nous devions modéliser une base de données et y importer des données depuis les données de Parcoursup contenue dans un fichier CSV. L’objectif était de pouvoir réduire le nombre de colonnes pour ne garder que les colonnes cohérentes et utiles (nous avons supprimer les colonnes qui pouvaient être recalculées comme les pourcentages par exemple). Enfin, nous devions réaliser des requêtes sur ces données. De plus, une partie statistiques était présente dans la SAE, nous devions répondre à des affirmation, soit en les confirmant soit en les démentant en utilisant des requêtes afin d’en extraire des données et de les analyser.
