/*
  Script contenant toutes les requetes presentes sur le rapport.

  Chaque requête est référencée par la question qui lui correspond.
  Les explications des requetes sont disponibles sur le rapports.

*/


/*
  Question 1: Requête affichant la colonne n56 et la re-calculant à partir d’autres colonnes.
*/

SELECT n56, n57+n58+n59 AS N56parcalcul  from import;

/*
  Question 2: Requête permettant de vérifier que le calcul de n56 est correct. 
*/

SELECT COUNT(n56)=(SELECT COUNT(*) from import) from import WHERE n56=n57+n58+n59;

/*
  Question 3: Requête affichant la colonne n74 et la re-calculant à partir d’autres colonnes.
*/

SELECT n74, ROUND(n51*100/nullif(n47,0)) AS calcul from import; 

/*
  Question 4: Requête permettant de vérifier que le calcul de n74 est correct.
*/

SELECT COUNT(n74)=(SELECT COUNT(*) from import) FROM import WHERE n74=ROUND(n51*100/nullif(n47,0)) OR ROUND(n51*100/nullif(n47,0)) IS NULL;


/*
  Question 5: Requête affichant la colonne n76 et la re-calculant à partir d’autres colonnes.
*/

SELECT n76, ROUND(n53*100/nullif(n47,0)) AS calcul from import;


/*
  Question 6: Même requête que la précédente sur les tables ventilées.
*/

SELECT coalesce(ROUND(admisEnProcedurePrincipale*100/nullif(totalAdmis,0)),0) AS n76_Recalcul from admis ;

/*
  Question 7: Requête affichant la colonne n81 et le recalcul de celle-ci a partir de la table import.
*/

SELECT n81, ROUND(n55*100.0/nullif(n56,0)) from import;

/*
  Question 8: Même requête que la précédente sur les tables ventilées.
*/

SELECT coalesce(ROUND(admisBoursiers*100.0/nullif(admisGeneraux+admisPro+admistechno,0)), 0) from admis;