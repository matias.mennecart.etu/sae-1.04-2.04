/*
    Suppression des tables si elles existes deja (permet de pouvoir faire tourner le sript plusieurs fois et d'obtenir le meme resultat).
*/

DROP TABLE import;
DROP TABLE departements CASCADE;
DROP TABLE fillieres CASCADE;
DROP TABLE etablissements CASCADE;
DROP TABLE candidats CASCADE;
DROP TABLE admis CASCADE;

/*
    Importation des données depuis parcoursup. (téléchargement du fichier et suppression de la premiere ligne)
*/ 

/*
\! curl 'https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-parcoursup/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B' -o dataDL.csv;
\! sed 1d dataDL.csv > data.csv
*/
/*
 Création du fichier dico.csv depuis dataDL.csv
*/
/*
\! ./dico.bash  
*/

/*
    Création de la table import
*/

CREATE temp TABLE import (
    n1 int, n2 text, n3 varchar(8), n4 text, n5 varchar(3), n6 text, n7 text, n8 text, n9 text,
    n10 text, n11 text, n12 text, n13 text, n14 text, n15 text, n16 text, n17 text,
    n18 int, n19 int, n20 int, n21 int, n22 int, n23 int, n24 int, n25 int, n26 int,
    n27 int, n28 int, n29 int, n30 int, n31 int, n32 int, n33 int, n34 int, n35 int,
    n36 int, n37 int, n38 int, n39 int, n40 int, n41 int, n42 int, n43 int, n44 int,
    n45 int, n46 int, n47 int, n48 int, n49 int, n50 int, n51 float, n52 float, n53 float,
    n54 int, n55 int, n56 int, n57 int, n58 int, n59 int, n60 int, n61 int, n62 int,
    n63 int, n64 int, n65 int, n66 float, n67 int, n68 int, n69 int, n70 int, n71 int,
    n72 int, n73 int, n74 numeric(3), n75 numeric(3), n76 numeric(3), n77 numeric(3),
    n78 numeric(3), n79 numeric(3), n80 numeric(3), n81 numeric(3), n82 numeric(3),
    n83 numeric(3), n84 numeric(3), n85 numeric(3), n86 numeric(3), n87 numeric(3),
    n88 numeric(3), n89 numeric(3), n90 numeric(3), n91 numeric(3), n92 numeric(3),
    n93 numeric(3), n94 numeric(3), n95 float, n96 float, n97 float, n98 float, n99 float, n100 float,
    n101 float, n102 text, n103 float, n104 text, n105 int, n106 text, n107 int, 
    n108 text, n109 text, n110 int, n111 text, n112 text, n113 numeric(3),
    n114 numeric(3), n115 numeric(3), n116 numeric(3), n117 char(5), n118 char(5)
);

/*
    Importation des données dans la table import depuis le fichier data.csv 
*/

\copy import from 'data.csv' delimiter ';' null '' encoding 'utf-8'

/*
    Création de la table départements
*/

CREATE TABLE departements (did varchar(3), dname text, academies text, rname text,
                            CONSTRAINT pk_departements PRIMARY KEY (did));

/*
    Insertion des données dans departements depuis les données de la table import.
*/

INSERT INTO departements (did, dname, academies, rname) SELECT DISTINCT n5, n6, n8, n7 from import AS I;

/*
    Création de la table fillieres
*/

CREATE TABLE fillieres (fid SERIAL, detaillée text, fname text, detaillée_bis text, tres_detaillée text, selectivité text,
                        CONSTRAINT pk_fillieres PRIMARY KEY (fid));

/*
    Insertion des données dans fillieres depuis les données de la table import.
*/

INSERT INTO fillieres (detaillée, fname, detaillée_bis, tres_detaillée, selectivité) SELECT DISTINCT n13, n14, n15, n16, n11  from import;

/*
    Création de la table etablissements
*/

CREATE TABLE etablissements (code_UAI varchar(8), statut text, ename text, did varchar(3), 
                            CONSTRAINT pk_etablissements PRIMARY KEY (code_UAI),
                            CONSTRAINT fk_etablissements FOREIGN KEY (did) REFERENCES departements(did));


/*
    Insertion des données dans etablissements depuis les données de la table import.
*/

INSERT INTO etablissements(code_UAI, statutfo, ename, did) SELECT DISTINCT n3,n2,n4,n5 FROM import;


/*
    Le choix des colonnes ajoutées est completement arbitraire.
    Les colonnes qui sont en poucentages sur le fichier data.csv n'ont pas besoin d'etre ajoutées car elles
    peuvent etre re calculés a tout moment.
*/

/*
    Création de la table candidats
*/

CREATE TABLE candidats (formationID int, code_UAI varchar(8), fid int, capacité_max int, candidats int, dontCandidates int, candidatsBoursier int, candidatsGeneraux int, candidatsTechno int, candidatsPro int, candidatsAutre int,
                            CONSTRAINT pk_candidats PRIMARY KEY (formationID),
                            CONSTRAINT fk_candidats FOREIGN KEY (code_UAI)
                                REFERENCES etablissements(code_UAI),
                            CONSTRAINT fk_candidats2 FOREIGN KEY (fid) REFERENCES fillieres(fid));

/*
    Insertion des données dans candidats depuis les données de la table import, fillieres.
*/


INSERT INTO candidats (formationID, code_UAI, fid, capacité_max, candidats, dontCandidates, candidatsBoursier, candidatsGeneraux, candidatsTechno, candidatsPro, candidatsAutre) SELECT I.n110, I.n3, F.fid, I.n18, I.n19, I.n20, (I.n24+I.n26+I.n28), (I.n23+I.n31), (I.n25+I.n32), (I.n27+I.n33), (I.n29+I.n34) FROM import AS I, fillieres as F WHERE I.n14 IS NOT DISTINCT FROM F.fname AND I.n13 IS NOT DISTINCT FROM F.detaillée AND I.n15 IS NOT DISTINCT FROM F.detaillée_bis AND I.n16 IS NOT DISTINCT FROM F.tres_detaillée;

/*
    Création de la table admiss
*/


CREATE TABLE admis (formationID int, code_UAI varchar(8), fid int, capacité_max int, totalAdmis int, dontAdmises int, admisBoursiers int, admisGeneraux int, admisTechno int, admisPro int, admisAutre int, admisLocaux int, admisEnProcedurePrincipale int, 
                            CONSTRAINT pk_admis PRIMARY KEY (formationID),
                            CONSTRAINT fk_admis FOREIGN KEY (code_UAI)
                                REFERENCES etablissements(code_UAI),
                            CONSTRAINT fk_admis2 FOREIGN KEY (fid) REFERENCES fillieres(fid));
/*
    Utilisation de IS NOT DISTINCT FROM remplace le = qui ne fonctionne pas sur des valeurs null
    Celui-ci est cependant un peu moins efficaces qu'un =, mais étant donnée que la création ne s'execute 
    qu'une seule fois, cela n'est pas vraiment un probleme.
*/

/*
    Insertion des données dans admis depuis les données de la table import, fillieres.
*/

INSERT INTO admis(formationID, code_UAI, fid, capacité_max, totalAdmis, dontAdmises, admisBoursiers, admisGeneraux, admisTechno, admisPro, admisAutre, admisLocaux, admisEnProcedurePrincipale) SELECT I.n110, I.n3, F.fid, I.n18, I.n47, I.n48, I.n55, I.n57, I.n58, I.n59, I.n60, I.n72, I.n53 FROM import AS I, fillieres as F WHERE I.n14 IS NOT DISTINCT FROM F.fname AND I.n13 IS NOT DISTINCT FROM F.detaillée AND I.n15 IS NOT DISTINCT FROM F.detaillée_bis AND I.n16 IS NOT DISTINCT FROM F.tres_detaillée;
